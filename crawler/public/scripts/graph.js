
var Chart = require('chart.js');

var jsonData = new XMLHttpRequest();
jsonData.open("GET", 'dados.json', false)

monthToShow = month.value;

var jsonString = '';
jsonData.onreadystatechange = function ()
    {
        if(jsonData.readyState === 4)
        {
            if(jsonData.status === 200 || jsonData.status == 0)
            {
                jsonString = jsonData.responseText;
            }
        }
    }
jsonData.send(null);

var dataObject = JSON.parse(jsonString)

var cotdate = [];
var cotvalue = [];

console.log(dataObject)

for(var x in dataObject){
    if(dataObject[x].date.month == monthToShow){
        cotdate.push(dataObject[x].stringdate);
        cotvalue.push(dataObject[x].value);  
    }	
}

console.log(cotdate.length)

if(cotdate.length == 0){
    alert("Não há dados deste mês ainda! Clique em voltar.")
}
else {
    var ctx = document.getElementById('ctx');
    Chart.defaults.global.defaultFontColor = 'white';
    var myChart = new Chart(ctx, {
        type: 'line',
        data: {
            labels: cotdate,
            datasets: [{
                label: 'Valor R$',
                data: cotvalue,
                backgroundColor: 'rgba(0, 255, 0, 0.2',
                borderColor: 'rgba(0, 255, 0, 1)',
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            },
            responsive: true,
            maintainAspectRatio: false,
            legends: {
                label: {
                    fontColor: '#fff'
                }
            }
        }
    });
}