var jsonData = new XMLHttpRequest();
jsonData.open("GET", 'dados.json', false)

monthToShow = month.value;

var jsonString = '';
jsonData.onreadystatechange = function ()
    {
        if(jsonData.readyState === 4)
        {
            if(jsonData.status === 200 || jsonData.status == 0)
            {
                jsonString = jsonData.responseText;
            }
        }
    }
jsonData.send(null);

var dataObject = JSON.parse(jsonString)

var cotdate = [];
var cotvalue = [];

console.log(dataObject)

for(var x in dataObject){
    if(dataObject[x].date.month == monthToShow){
        cotdate.push(dataObject[x].stringdate);
        cotvalue.push(dataObject[x].value);  
    }
}

linhasTags = []
for(var x in cotdate){
	trLinhasTags = document.createElement('tr')
	
	var col1 = document.createElement('td')
	var text = document.createTextNode(cotdate[x])
	col1.appendChild(text)
    col1.setAttribute('class', 'col1')

	trLinhasTags.appendChild(col1)
	
	var col2 = document.createElement('td')
    col2.setAttribute('class', 'col2')
	var text = document.createTextNode('R$ '+String(cotvalue[x]).replace('.', ','))
	col2.appendChild(text)

	trLinhasTags.appendChild(col2)

	table.appendChild(trLinhasTags)
}

