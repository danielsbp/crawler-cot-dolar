let app = document.querySelector("#app")
let buttons = document.querySelector("#buttons");

let month = document.createElement("select");

let table;
let voltarButton;

monthOptions = [];
monthNames = ['Janeiro', 'Fevereiro', 'Março', 'Abril', 'Maio', 'Junho', 'Julho', 'Agosto', 'Setembro', 'Outubro', 'Novembro', 'Dezembro'];

for (var i = 0; i <= 11; i++) {
	let monthOption = document.createElement("option")
	monthOption.setAttribute("value", i+1);
	text = document.createTextNode(monthNames[i]);
	monthOption.appendChild(text);

	month.appendChild(monthOption);

	monthOptions[i] = monthOption;
}

let showButton = document.createElement("button")

text = document.createTextNode("Visualizar Dados");
showButton.appendChild(text);

let graphButton = document.createElement("button");

text = document.createTextNode("Ver gráfico");
graphButton.appendChild(text);

function renderButtons() {
	app.appendChild(month)	
	app.appendChild(showButton)
	app.appendChild(graphButton)
}

function removeElements(local) {
	if (local == 'show') {
		app.removeChild(table);
		app.removeChild(voltarButton);
	}
	else if(local == 'graph') {
		app.removeChild(canvasTag);
		app.removeChild(voltarButton);
	}
	else if(local == '.') {
		app.removeChild(month);
		app.removeChild(showButton);
		app.removeChild(graphButton);
	}
	else {
		console.warn("Remoção de elementos com argumento inválido.")
	}
	
}

renderButtons();
month.disabled = true;
showButton.disabled = true;
graphButton.disabled = true;

//Executa o web crawler em python
const { spawn } = require('child_process');
const python = spawn('python', [__dirname+'/python/script.py']);
python.stdout.on('data', (data) => {
	console.log(`stdout: ${data}`);
	month.disabled = false;
	showButton.disabled = false;
	graphButton.disabled = false;
});

python.stderr.on('data', (data) => {
	console.error(`stderr: ${data}`);
});

python.on('close', (code) => {
	console.log(`child process exited with code ${code}`);
});

function createBackButton(local) {
	voltarButton = document.createElement("button");
	var text = document.createTextNode("Voltar")
	voltarButton.appendChild(text)

	voltarButton.addEventListener('click', function(){
		removeElements(local)
		renderButtons()
	})

	return voltarButton;
}

graphButton.addEventListener('click', function(){
	canvasTag = document.createElement("canvas");
	canvasTag.setAttribute('id', 'ctx');

	canvasTag.setAttribute('width', 100)
	canvasTag.setAttribute('height', 100)

	var voltarButton = createBackButton('graph');

	removeElements('.');

	app.appendChild(canvasTag)
	app.appendChild(voltarButton)

	script = document.createElement('script')
	script.setAttribute('src', 'public/scripts/graph.js')

	app.appendChild(script)
})

showButton.addEventListener('click', function(){
	script = document.createElement('script')
	script.setAttribute('src', 'public/scripts/show.js')

	removeElements('.')

	table = document.createElement('table');
	var firstLine = document.createElement('tr');

	var tdDate = document.createElement('td');
	var text = document.createTextNode('DATA')
	tdDate.setAttribute('class', 'tLabel')
	tdDate.appendChild(text)

	var tdValue = document.createElement('td');
	var text = document.createTextNode('VALOR')
	tdValue.setAttribute('class', 'tLabel')
	tdValue.appendChild(text)

	firstLine.appendChild(tdDate)
	firstLine.appendChild(tdValue)

	table.appendChild(firstLine)

	app.appendChild(table)
	voltarButton = createBackButton('show');
	app.appendChild(voltarButton)

	app.appendChild(script)


})


