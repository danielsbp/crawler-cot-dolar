import requests
import json
from bs4 import BeautifulSoup

class dolar_cot_crawler(object):
	"""docstring for crawler"""
	def __init__(self):
		self.link = "https://www.dolarhoje.net.br/dolar-comercial/"
		self.month = []
		self.data = []
	def tela_inicial(self):
		print("-=-"*15)
		print("Cotação do Dólar de 2021")
		print("-=-"*15)

	def capturar_dados(self):

		self.tela_inicial()
		site = requests.get(self.link)

		soup = BeautifulSoup(site.content, 'html.parser')

		content_tag = soup.find_all('p')
		#content_tag = soup.find_all('div', {'class': 'e'})
		content = []
		for data in content_tag:
			content.append(str(data.contents[0]))
			#try:

			#except
		month_tags = filter(lambda x: "strong" in x, content)

		month_names = []

		for month_name in month_tags:
			cut1 = month_name.replace("<strong>", "")
			cut2 = cut1.replace("</strong>", "")
			month_names.append(cut2)

		self.month = month_names

		dolar_cot = filter(lambda x: "R$" in x, content)
		
		#separar dia, mês e ano
		line_data = []
		for dolar_cot_data in dolar_cot:
			
			#Pegando a data
			stringdate = dolar_cot_data[:10]
			day = int(stringdate[:2])
			month = int(stringdate[3:5])
			year = int(stringdate[6:10])

			#Pegando o valor:
			stringvalue = dolar_cot_data[13:22]
			value = float(stringvalue[3:].replace(",", "."))

			line_data.append({
				"date": {
					"day": day,
					"month": month,
					"year": year
				},
				"stringdate": stringdate,
				"value": value,
				"stringvalue": stringvalue
			})

		self.data = line_data

		print("Salvando os dados em JSON...")
		with open('dados.json', 'w') as f:
			json.dump(self.data, f, indent = 4, sort_keys=True)
		print("Dados salvos com sucesso!")

			
crawler = dolar_cot_crawler();

crawler.capturar_dados()


#print(crawler.data)