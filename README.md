# DCC - Web Crawler para Cotação do Dolar

### Português-BR
## Sobre
O DCC é um software cuja intenção é mostrar a cotação do dolar através de duas visualizações: Gráfico e Tabela. Ele foi criado com o objetivo de treinar e estudar maneiras de fazer a comunicação entre Javascript e Python. 

## Como funciona?
Quando o software é aberto, ele executa um script de Web Crawler feito em Python que captura os dados do site [DólarHoje](https://www.dolarhoje.net.br/dolar-comercial/). Logo após, ele armazena os dados em um arquivo JSON e então a tela é liberada para que o usuário visualize os dados. A interface gráfica é feita em HTML, CSS e Javascript, rodando com o framework Electron. Para a renderização do gráfico, foi usada uma biblioteca de Javascript chamada Chart.js. 

## Como rodar?
Você precisa da versão v14.15.4 ou próximas do [Node.js](https://nodejs.org/en/download/) e do [Python](https://www.python.org/downloads/) 3.8.5. Além das duas ferramentas citadas, será necessário baixar duas bibliotecas Python: [Requests](https://requests.readthedocs.io/en/master/) e [BeautifulSoup4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/).
Para instalar, é só executar os seguintes comandos:

`pip install requests`

`pip install BeautifulSoup4`

Logo após a instalação de todas essas ferramentas, entre na pasta que você salvou todos os arquivos, abra o terminal nela e digite: `npm start`

### English:

## About
DCC is a software who the objective is show the dolar cotation through two views: Graph and Table. It was created with the goal of training and studying ways to communicate between Javascript and Python. 

## How it works?
When the software is opened, it runs a Web Crawler script made in Python that captures data from the [DólarHoje](https://www.dolarhoje.net.br/dolar-comercial/) website. Soon after, it stores the data in a JSON file and then the screen is released for the user to view the data. The graphical interface is made from HTML, CSS and Javascript, running on the Electron framework. For rendering the graph, a Javascript library called Chart.js was used. 

## How to execute?
You need [Node.js](https://nodejs.org/en/download/)  v14.15.4 or later and [Python](https://www.python.org/downloads/) 3.8.5. In addition to the two tools mentioned above, you will need to download two Python libraries: [Requests](https://requests.readthedocs.io/en/master/) and [BeautifulSoup4](https://www.crummy.com/software/BeautifulSoup/bs4/doc/).
For install, execute the commands bellow on your terminal:

`pip install requests`

`pip install BeautifulSoup4`

Once you have installed all these tools, go to the folder where you saved all the files, open the terminal there and type: `npm start`.